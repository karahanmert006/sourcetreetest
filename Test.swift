

import Foundation
import UIKit

final class Test {
    var age: Int = 0
    var name: String = ""
    var surname: String = ""
}

struct Brand {
    var first: String = "Mercedes"
    var second: String = "Bmw"
    var third: String = "Audi"
    var fourth: String = "Opel"
    var fifth: String = "Ferrari"
    var csa: String = "Ford"
    var acsad: String = "Tesla"
}

class Human {
    var first: String = "Servet"
    var second: String = "Berk"
    var third: String = "Selim"
    var fourth: String = "ad"
}

class Pc {
    var first: String = "Apple"
    var second: String = "Linux"
    var third: String = "Windows"
}

struct lastStruct {
    var lastStructProp: String = "LastStructProp"
}

class lastClass {
    var first: String = "Last"
    var second: Int = 12
}

class Motorcycle {
    var firstt: String = "Ducati"
}
